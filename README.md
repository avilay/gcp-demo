# GCP-DEMO
This is a group of demo micro-services that can be deployed on GCP using GAE. Ideally each micro-service would be in its own repo but for the sake of browsing convenience I have put each micro-service in a separate directory. This is why each directory has its own `.gitignore` and `requirements.txt`.

At a high level, these micro-services maintain a collection of podcasts that users can browse and add new podcasts to. There are three basic components, an API to browse and add podcasts, a database where the podcasts are stored, and abatch job that crawls the podcast URLs for new episodes periodically.

![Architecture](arch.png)

### Podcasts DB
This is the datastore that holds all the podcasts the system knows about, and their episodes. This has been implemented as a GCP managed PostgreSQL RDBMS for no other reason than ease of use. This component is not very relevant to the micro-services part of the demo. As this is a managed GCP service, there is no code or a corresponding directory for this. The instructions on how to set this up are in the deployment section below.

### Crawler
This component gets all the podcast feed URLs from the DB, crawls the feeds and updates the DB with any new episodes. This is a batch job that is supposed to run with some fixed periodicity, typically once a day. When implemented as GAE, the crawler has to be wrapped in an API that only GAE cron jobs can call. This component has two different implementations. In one implementation it is bundled with the REST APIs. This is for the scenario where the batch job does not need a custom Docker container but can run in the base GAE container. The other implementation is when the crawler needs to run code in a custom Docker container.

### APIs
This provides a set of REST APIs to get podcasts and specific episodes from a podcast.

### Authentication
#### REST APIs
There are a number of ways in which REST APIs can be secured at the Cloud Endpoint. The scheme demonstrated here is the simplest. Authentication is provided by GCP infrastructure, but the authorization is left upto the service code. Any service account in *any* GCP project can have Google verify and sign its identity. Once it gets a signed token from Google, it can make a request to the Podcasts REST APIs. Then it is upto the REST API to decide whether the caller should be allowed to proceed or not.

#### Cron Jobs
The GAE cron infrastructure periodically calls the configured cron jobs with a special header. If it receives an external call with this header, it will strip it out. So the service can rest assured that when this header is present in the request, the requestor is indeed GAE cron. This way of authentication and authorization is demonstrated in the code here.

## Services
This repo contains code for two micro-services `podcasts` and `crawler` and a third directory containing client code that is able to make authenticated calls to the `podcasts` API. The rest of this document explains each of these code bases and how to deploy and run them.

### `podcasts-service`
This is the micro-service that contains the REST APIs to add to and browse all the existing podcasts and their episodes. This is deployed as a Google Endpoint Open API backed by a Flask GAE app. There is an optional `crawler` component built into this service for the scenario where the `crawler` can run on a basic Flex GAE Docker image.

### `crawler-service`
The scenario for this service is when we already have a batch job that runs inside a Docker container that we were already running somewhere else but now want to run it GCP cloud. This is run as a GAE cron. All GAE apps have to be wrapped inside a WSGI web server. In order to do that there is a light-weight Flask app that simply calls the existing process. Both the existing crawler process and the Flask API will be run in the custom Docker container.

### `podcasts-client`
Based on the REST API security scheme described above, this code downloads a service account key file from Cloud Storage, asks Google to sign it, and then makes a request to the Podcast APIs using this signed ID token.

## Deployment
### Create GCP Project
Create a GCP project and enable the following APIs:
  * Cloud SQL Admin API
  * Google Cloud SQL
  * Cloud Endpoints Portal
  * Google Cloud Endpoints
  * Google App Engine Flexible Environment

### Create GCP PostgreSQL
Create a PostgreSQL instance in GCP and run the `create.sql` script by following the instructions in [GCP docs](https://cloud.google.com/sql/docs/postgres/).
TODO

### Setup PROJECT-ID
Add project specific information to these config files:
  * app.yaml: Grab your SQL instance connection name from GCP and replace {YOUR-GCP-POSTGRES-INSTANCE-CONN-NAME}.
  * swagger.yaml: Get your project ID and replace {YOUR-PROJECT-ID-HERE} in line 6.

### Setup Project Metadata
Create a key called `podcasts_config` and set its value to the following JSON in your project metadat.

```
{
    "POSTGRES": {
        "host": "/cloudsql/<instance conn name>,
        "user": "postgres",
        "password": <password here>,
        "database": "firweed"
    }
}
```
Instructions for how to setup project metadata can be found [here](https://cloud.google.com/compute/docs/storing-retrieving-metadata#projectwide).

### Deploy App
Deploy the Flask app as the `podcast` service.
```
$ cd gcp-demo/podcasts-service
$ gcloud app deploy
```

### Deploy Endopint API
```
$ cd gcp-demo/podcasts-service
$ gcloud endpoints services deploy swagger.yaml
```

### Deploy cron
#### As part of `podcasts` service
```
$ cd gcp-demo/podcasts-service
$ gcloud app deploy cron.yaml
```

#### As its own `crawler` service
```
$ cd gcp-demo/crawler-service
$ gcloud app deploy
$ gcloud app deploy cron.yaml
```

### Call API
```
$ cd gcp-demo/podcasts-client
$ python client.py
```

## Quick Notes
The first GAE service to be deployed has to be the `default` service. I cannot deploy `podcast` service first. For this I have creted a `hello-service` that can be deployed as the `default` service.

When creating a db remember to set the default user's (postgres) password. I cannot connect to the GCP SQL with only a default account, i.e., no password account.

To connect to PG from my local dev env I need to start a proxy through which all db connections will be routed.
Instructions to download and run the proxy at https://cloud.google.com/sql/docs/postgres/quickstart-proxy-test. I can then start my local pgAdmin by giving the server address as localhost and the port as 5432 (if I followed the same port as mentioned in the linked doc).

Remember to create a default network in the GCP project because the one created by emporium will have no networks.