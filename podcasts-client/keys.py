import json
from urllib.parse import urlparse
from pathlib import Path
from google.cloud import storage


def get_svc_acc_key(keypath):
    if keypath is None:
        return get_key_from_metadata()
    urlparts = urlparse(keypath)
    if urlparts.scheme == 'gcs':
        pathparts = Path(urlparts.path).parts
        project = urlparts.netloc
        bucket = pathparts[1]
        key = pathparts[2]
        return download_key(project, bucket, key)
    elif urlparts.scheme == 'file':
        return read_key(urlparts.path)


def get_key_from_metadata():
    raise NotImplementedError()


def read_key(filepath):
    with open(filepath, 'rt') as f:
        key = json.load(f)
    return key


def download_key(project, bucket, key):
    sc = storage.Client(project=project)
    bucket = sc.get_bucket(bucket)
    blob_client = bucket.blob(key)
    blob = blob_client.download_as_string(sc)
    key = json.loads(blob.decode('utf-8'))
    return key
