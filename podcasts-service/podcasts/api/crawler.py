import logging

from podcasts.parser import InvalidFeed, InvalidPodcast, InvalidUrl, parse

logger = logging.getLogger(__package__)


def crawl(ds):
    podcasts = ds.get_all_podcasts()
    num_new_episodes = 0
    for podcast in podcasts:
        logger.info(f'Crawling id {podcast.podcast_id} {podcast.feed_url}')
        try:
            pc, eps = parse(podcast.feed_url)
            pc.podcast_id = podcast.podcast_id
            ds.update_podcast(pc)
            logger.info(f'Found {len(eps)} episodes in {podcast.podcast_id}')
            for ep in eps:
                if ds.get_episode_by_guid(ep.guid) is None:
                    logger.debug(f'Found new episode {ep.guid} for {podcast.podcast_id}')
                    num_new_episodes += 1
                    ep.podcast_id = podcast.podcast_id
                    ds.add_episode(ep)
        except InvalidPodcast as ip:
            logger.warn(f'Feed {podcast.feed_url} does not have any podcasts!')
        except InvalidFeed as ifd:
            logger.warn(f'{podcast.feed_url} is not a valid RSS/ATOM feed!')
        except InvalidUrl as iu:
            logger.warn(f'{podcast.feed_url} is not a valid url!')
    ds.add_crawl_history(len(podcasts), num_new_episodes)
