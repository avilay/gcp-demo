import requests
from google.auth import jwt, crypt
from google.oauth2 import service_account
from datetime import datetime
import keys

OAUTH_TOKEN_URL = 'https://www.googleapis.com/oauth2/v4/token'
# SERVICE_HOST = 'podcast-dot-unity-ml-gcp-demo-test.appspot.com'
SERVICE_HOST = 'messenger.demo.ml.unity3d.com'
SERVICE_URL = f'https://{SERVICE_HOST}'

def gen_self_signed_jwt_for_svc(key):
    signer = crypt.RSASigner.from_service_account_info(key)
    now = int(datetime.now().timestamp())
    expires = now + 3600
    payload = {
        'iat': now,
        'exp': expires,
        'aud': SERVICE_HOST,
        'iss': key['client_email'],
        'sub': key['client_email'],
        'email': key['client_email']
    }
    jwtoken = jwt.encode(signer, payload).decode('utf-8')
    return jwtoken


def gen_self_signed_jwt_for_goog(key):
    if key is None:
        raise ValueError('Key cannot be None!')
    now = int(datetime.now().timestamp())
    signer = crypt.RSASigner.from_service_account_info(key)
    expires = now + 3600
    payload = {
        'iat': now,
        'exp': expires,
        'scope': '',
        'iss': key['client_email'],
        'aud': OAUTH_TOKEN_URL,
        'target_audience': SERVICE_URL
    }
    jwtoken = jwt.encode(signer, payload)
    return jwtoken


def gen_self_signed_jwt_for_goog_alt(key):
    creds = jwt.Credentials.from_service_account_info(key, audience=OAUTH_TOKEN_URL)
    svc_creds = service_account.Credentials(
        creds.signer, creds.signer_email,
        token_uri=OAUTH_TOKEN_URL,
        additional_claims={
            'target_audience': SERVICE_URL
        }
    )
    jwtoken = svc_creds._make_authorization_grant_assertion()
    return jwtoken


def get_goog_signature(jwtoken):
    grant_type = 'urn:ietf:params:oauth:grant-type:jwt-bearer'
    params = {
        'assertion': jwtoken,
        'grant_type': grant_type
    }
    resp = requests.post(OAUTH_TOKEN_URL, data=params)
    return resp.json()['id_token']


def make_call(jwtoken, path):
    headers = {'Authorization': f'Bearer {jwtoken}'}
    url = f'{SERVICE_URL}{path}'
    print(f'Making call to {url}')
    resp = requests.get(url, headers=headers)
    print(f'\nResponse Status: {resp.status_code}')
    print(resp.json())


def in_gcp():
    return False


def main():
    keypath = None
    if not in_gcp():
        keypath = input('Full URL of key file (e.g., file:///path/to/key.json): ')
    path = input('Service url path not including the hostname (e.g., /podcasts): ')
    issuer = input('JWT issuer (google/self): ')

    print('Getting key json')
    svc_acc_key = keys.get_svc_acc_key(keypath)

    print('Generating JWT')
    if issuer == 'google':
        self_signed_jwt = gen_self_signed_jwt_for_goog(svc_acc_key)
        jwt = get_goog_signature(self_signed_jwt)
    else:
        jwt = gen_self_signed_jwt_for_svc(svc_acc_key)
        print(jwt)

    print('Making the API call')
    make_call(jwt, path)


if __name__ == '__main__':
    main()
