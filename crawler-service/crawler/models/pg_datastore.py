import logging
from copy import deepcopy
from datetime import datetime, timezone

import psycopg2 as pg
import psycopg2.extras as pgextras

from .datastore import Datastore, EntityNotFoundError
from .episode import Episode
from .podcast import Podcast

logger = logging.getLogger(__package__)


class PgDatastore(Datastore):
    def __init__(self, config):
        self._pgconfig = config['POSTGRES']
        self._db = None
        self._connect()

    def _connect(self):
        self._db = pg.connect(**self._pgconfig)
        self._db.autocommit = True
        logger.info('DB connection established')

    def close(self):
        if self._db and self._db.closed == 0:
            self._db.close()
        logger.info('DB connection closed')

    def _cursor(self):
        if self._db and self._db.closed != 0:
            logger.warn('PG connection is broken/closed. Re-connecting.')
            self._connect()
        return self._db.cursor(cursor_factory=pgextras.RealDictCursor)

    def add_podcast(self, podcast: Podcast):
        sql = '''
        INSERT INTO podcasts (link, title, description, author, image_url, feed_url, published_on)
        VALUES (%s, %s, %s, %s, %s, %s, %s)
        RETURNING *
        '''
        params = [
            podcast.link,
            podcast.title,
            podcast.description,
            podcast.author,
            podcast.image_url,
            podcast.feed_url,
            podcast.published_on
        ]
        with self._cursor() as cur:
            cur.execute(sql, params)
            new_podcast = deepcopy(podcast)
            new_podcast.podcast_id = cur.fetchone()['podcast_id']
        logger.info(f'Added new podcast {podcast.title}')
        return new_podcast

    def update_podcast(self, podcast: Podcast):
        sql = '''
        UPDATE podcasts
        SET link = %s, title = %s, description = %s, author = %s, image_url = %s, published_on = %s
        WHERE podcast_id = %s
        '''
        params = [
            podcast.link,
            podcast.title,
            podcast.description,
            podcast.author,
            podcast.image_url,
            podcast.published_on,
            podcast.podcast_id
        ]
        with self._cursor() as cur:
            cur.execute(sql, params)
        logger.info(f'Updated podcast {podcast.podcast_id}')

    def add_episode(self, episode: Episode):
        sql = '''
        INSERT INTO episodes (podcast_id, guid, title, published_on, description, podcast_url, image_url, web_url)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
        RETURNING *
        '''
        params = [
            episode.podcast_id,
            episode.guid,
            episode.title,
            episode.published_on,
            episode.description,
            episode.podcast_url,
            episode.image_url,
            episode.web_url
        ]
        with self._cursor() as cur:
            cur.execute(sql, params)
            new_episode = deepcopy(episode)
            new_episode.episode_id = cur.fetchone()['episode_id']
        return new_episode

    def get_podcast(self, podcast_id):
        sql = '''
        SELECT podcast_id, link, title, description, author, image_url, feed_url, published_on
        FROM podcasts
        WHERE podcast_id = %s
        '''
        podcast = None
        with self._cursor() as cur:
            cur.execute(sql, [podcast_id])
            if cur.rowcount == 0:
                raise EntityNotFoundError(f'Podcast id {podcast_id} not found!')
            podcast = Podcast(**cur.fetchone())
        return podcast

    def get_podcast_by_feed_url(self, feed_url):
        sql = '''
        SELECT podcast_id, link, title, description, author, image_url, feed_url, published_on
        FROM podcasts
        WHERE feed_url = %s
        '''
        podcast = None
        with self._cursor() as cur:
            cur.execute(sql, [feed_url])
            if cur.rowcount == 1:
                podcast = Podcast(**cur.fetchone())
        return podcast

    def get_all_podcasts(self):
        sql = '''
        SELECT podcast_id, link, title, description, author, image_url, feed_url, published_on
        FROM podcasts
        '''
        podcasts = []
        with self._cursor() as cur:
            cur.execute(sql)
            podcasts = [Podcast(**row) for row in cur.fetchall()]
        return podcasts

    def get_episodes(self, podcast_id):
        sql = '''
        SELECT episode_id, podcast_id, title, published_on, description, podcast_url, image_url, web_url
        FROM episodes
        WHERE podcast_id = %s
        '''
        episodes = []
        with self._cursor() as cur:
            cur.execute(sql, [podcast_id])
            if cur.rowcount == 0:
                raise EntityNotFoundError(f'Episodes for podcast {podcast_id} do not exist!')
            episodes = [Episode(**row) for row in cur.fetchall()]
        return episodes

    def get_episode(self, podcast_id, episode_id):
        sql = '''
        SELECT episode_id, podcast_id, title, published_on, description, podcast_url, image_url, web_url
        FROM episodes
        WHERE podcast_id = %s AND episode_id = %s
        '''
        episode = None
        with self._cursor() as cur:
            cur.execute(sql, [podcast_id, episode_id])
            if cur.rowcount == 0:
                raise EntityNotFoundError(f'Episode {episode_id} for podcast {podcast_id} does not exist!')
            episode = Episode(**cur.fetchone())
        return episode

    def get_episode_by_guid(self, guid):
        sql = '''
        SELECT episode_id, podcast_id, title, published_on, description, podcast_url, image_url, web_url
        FROM episodes
        WHERE guid = %s
        '''
        episode = None
        with self._cursor() as cur:
            cur.execute(sql, [guid])
            if cur.rowcount == 1:
                episode = Episode(**cur.fetchone())
        return episode

    def add_crawl_history(self, num_podcasts, num_new_episodes):
        sql = '''
        INSERT INTO crawl_history (last_crawl_on, num_podcasts, num_new_episodes)
        VALUES (%s, %s, %s)
        '''
        with self._cursor() as cur:
            cur.execute(sql, [datetime.now(timezone.utc), num_podcasts, num_new_episodes])
