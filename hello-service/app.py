import logging
from flask import Flask, jsonify, request


app = Flask(__name__)
app.logger.setLevel(logging.DEBUG)
app.logger.info('Starting app')


@app.route('/namaste')
def get_hello():
    # msg = 'Hello default unity-ml-tobedeleted-test!'
    # msg = 'Hello hello service of unity-ml-tobedeleted-test!'
    # msg = 'Hello default gcp-demo!'
    msg = 'Namaste from Greet service of gcp-demo!'
    return jsonify({'message': msg})

@app.route('/info')
def get_info():
    msg = {
        'remote_addr': request.remote_addr,
        'user_agent': str(request.user_agent)
    }
    return jsonify(msg)
