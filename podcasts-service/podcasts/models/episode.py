from datetime import datetime


class Episode:
    def __init__(self, **kwargs):
        self.episode_id = kwargs.get('episode_id')
        self.podcast_id = kwargs.get('podcast_id')
        self.guid = kwargs.get('guid')
        self.title = kwargs.get('title')
        self.published_on = kwargs.get('published_on')
        if not self.published_on:
            self.published_on = datetime.min
        self.description = kwargs.get('description')
        self.podcast_url = kwargs.get('podcast_url')
        self.image_url = kwargs.get('image_url')
        self.web_url = kwargs.get('web_url')

    def to_dict(self):
        return dict(
            episode_id=self.episode_id,
            podcast_id=self.podcast_id,
            title=self.title,
            published_on=self.published_on.isoformat(),
            description=self.description,
            podcast_url=self.podcast_url,
            image_url=self.image_url,
            web_url=self.web_url
        )

    def __repr__(self):
        if len(self.description) > 20:
            desc = self.description[:20] + ' ...'
        else:
            desc = self.description
        return f'<Episode(episode_id={self.episode_id}, podcast_id={self.podcast_id}, ' + \
               f'title={self.title}, published_on={self.published_on.isoformat()}, description={desc} ' + \
               f'podcast_url={self.podcast_url}, image_url={self.image_url}, web_url={self.web_url})>'
